# Todo App

Helping you select an client framework. Inspired by https://github.com/tastejs/todomvc

- cross: Cross Platform
  - flutter
  - ionic
  - qt
  - react-native
  - web
    - react
- native: Native Platform
  - android
  - ios
  - linux
  - macos
  - windows
